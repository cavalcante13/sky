//
//  HomeViewController.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeViewController: UIViewController {
    @IBOutlet private weak var collectionView : UICollectionView!
    
    private var presenter : HomePresenter
    
    init() {
        self.presenter = HomePresenter()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.presenter = HomePresenter()
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.loadMoviesRequest()
    }
    
    private func setupLayout() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(class: HomeCollectionViewCell.self)
        
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl?.addTarget(self, action: #selector(loadMoviesRequest), for: .valueChanged)
    }
    
    
    @objc
    private func loadMoviesRequest() {
        //Show activity indicator
        ActivityIndicator.show()
        presenter.loadMovies(moviesLoadHandler(), failure: moviesLoadFailure())
    }
    
    private func moviesLoadHandler()-> Handler {
        return { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.collectionView.reloadData()
            strongSelf.collectionView.refreshControl?.endRefreshing()
            ActivityIndicator.stop()
        }
    }
    
    private func moviesLoadFailure()-> Failure {
        return { error in
            //Put error view
            ActivityIndicator.show(error: error?.domain ?? "")
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { [weak self] context in
            self?.collectionView?.collectionViewLayout.invalidateLayout()
            }, completion: nil)
    }
    
    deinit {
        print(self)
    }
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    //if change the orientation
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.sectionInset  = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        let columns: Int = {
            var count = 2
            if traitCollection.horizontalSizeClass == .regular              { count += 1 }
            if collectionView.bounds.width > collectionView.bounds.height   { count += 1 }
            return count
        }()
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(columns - 1))
        
        let width = Int((collectionView.bounds.width - totalSpace) / CGFloat(columns))
        return CGSize(width: CGFloat(width), height: traitCollection.verticalSizeClass == .compact ? UIScreen.main.bounds.size.height : UIScreen.main.bounds.size.height / 2)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(cell: HomeCollectionViewCell.self, indexPath: indexPath)
        cell.set(movie: presenter.movies[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let navigationController = MovieDetailNavigationController(presenter.movies[indexPath.item])
        
        present(navigationController, animated: true, completion: nil)
    }
}
