//
//  HomeNavigationController.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import UIKit

class HomeNavigationController: UINavigationController, UINavigationControllerDelegate {

    private var homeViewController : HomeViewController
    
    
    init() {
        self.homeViewController = HomeViewController()
        super.init(rootViewController: homeViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("this navigationController not supported instance by interface builder")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.homeViewController = HomeViewController()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationBar.isTranslucent = false
        navigationBar.tintColor     = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationBar.barTintColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationBar.shadowImage   = UIImage()
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18),
                                             NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)]
        
        navigationBar.prefersLargeTitles       = true
        navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 28),
                                                  NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)]
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        switch viewController {
        case is HomeViewController:
            viewController.navigationItem.title = "Sky Filmes"
        default:
            viewController.navigationItem.title = "Sky"
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
