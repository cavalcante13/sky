//
//  HomeCollectionViewCell.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import UIKit
import SDWebImage


final class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var coverMovieImageView : UIImageView!
    @IBOutlet private weak var yearMovieLabel : UILabel!
    @IBOutlet private weak var nameMovieLabel : UILabel!
    
    private var movie : Movie!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func set(movie : Movie) {
        self.movie = movie
        
        self.yearMovieLabel.text = movie.releaseYear
        self.nameMovieLabel.text = movie.title
        self.coverMovieImageView.sd_setImage(with: movie.coverUrl)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.yearMovieLabel.text = ""
        self.nameMovieLabel.text = ""
        self.coverMovieImageView.image = nil
    }
}
