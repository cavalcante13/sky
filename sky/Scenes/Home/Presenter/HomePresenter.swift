//
//  HomePresenter.swift
//  sky
//
//  Created by DiegoCosta on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import UIKit

class HomePresenter: NSObject {

    private(set) var movies : [Movie] = []
    
    override init() {
        super.init()
    }
    
    func loadMovies(_ completion : @escaping Handler, failure : Failure? = nil) {
        let requestable = MoviesRequestable()
        let request : HTTPRequest<[Movie]> = HTTPRequest(requestable: requestable)
        request.get(success: { response in
            guard let movies = response else {
                failure?(NSError(domain: "Ocorreu um erro", code: 0, userInfo: nil))
                return
            }
            
            self.movies = movies
            completion()
        }, failure: { error in
            failure?(error ?? NSError(domain: "Ocorreu um erro", code: 0, userInfo: nil))
        })
    }
}
