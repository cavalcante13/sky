//
//  MovieDetailNavigationController.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import UIKit

class MovieDetailNavigationController: UINavigationController, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    private var movieDetailViewController : MovieDetailViewController!
    
    
    init(_ movie : Movie) {
        self.movieDetailViewController = MovieDetailViewController(movie)
        super.init(rootViewController: movieDetailViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("this navigationController not supported instance by interface builder")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.transitioningDelegate = self
        self.modalPresentationStyle = .custom
        self.setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationBar.isTranslucent = true
        navigationBar.shadowImage   = UIImage()
        navigationBar.tintColor     = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        navigationBar.barTintColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationBar.shadowImage   = UIImage()
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18),
                                             NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)]
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.roundCorners([.topLeft, .topRight], radius: 8)
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if let rootViewController = navigationController.viewControllers.first {
            rootViewController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(unwind(_:)))
        }
    }
    
    @objc
    private func unwind(_ sender : Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch presented {
        case is MovieDetailNavigationController:
            let frame = CGRect(x: 8, y: view.frame.minY + 64, width: view.frame.width - 8, height: view.bounds.height)
            return UpTransiction(frame: frame)
        default:
            return nil
        }
    }
}
