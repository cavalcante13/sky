//
//  MoviewDetailViewController.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet private weak var scrollView : UIScrollView!
    @IBOutlet private weak var coverMovieImageView : UIImageView!
    @IBOutlet private weak var yearMovieLabel : UILabel!
    @IBOutlet private weak var overviewMovieTextView : UITextView!
    
    private var movie : Movie
    
    
    init(_ movie : Movie) {
        self.movie = movie
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.movie = Movie()
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = movie.title
        self.setupLayout()
    }
    
    private func setupLayout() {
        scrollView.delegate = self
        
        yearMovieLabel.text = movie.releaseYear
        overviewMovieTextView.text = movie.overview
        coverMovieImageView.sd_setImage(with: movie.coverUrl)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset    = scrollView.contentOffset.y + (navigationController?.navigationBar.bounds.height ?? 0.0)
        var transform = CATransform3DIdentity
        
        if offset < 0 {
            let scale : CGFloat = -(offset) / coverMovieImageView.bounds.height
            transform = CATransform3DScale(transform, 1.0 + scale, 1.0 + scale, 0)
            coverMovieImageView.layer.transform = transform
        }
    }
    
    deinit {
        print(self)
    }
}
