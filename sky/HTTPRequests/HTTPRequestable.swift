//
//  HTTPRequestable.swift
//  sky
//
//  Created by Diego Cavalcante on 22/08/2018.
//

import Foundation

protocol HTTPRequestable : HTTPDomain {
    
    var path : String { get }
    
    var url  : URL { get }
    
    var params : [String : Any] { get }
}

extension HTTPRequestable {
    
    var headers : [String : String] {
        return ["Content-Type" : "application/json", "Accept" : "application/json" ]
    }
}
