//
//  HTTPDomain.swift
//  sky
//
//  Created by Diego Cavalcante on 22/08/2018.
//

import Foundation


protocol HTTPDomain {
    
    var environment : HTTPEnvironment { get }
}

extension HTTPDomain {
    
    var environment : HTTPEnvironment {
        return .prod
    }
}
