//
//  MoviesRequestable.swift
//  sky
//
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import Foundation


struct MoviesRequestable: HTTPRequestable {
    
    var path: String {
        return "Movies"
    }
    
    var url: URL {
        return URL(string: environment.host + path)!
    }
    
    var params: [String : Any] {
        return [:]
    }
}
