//
//  HTTPEnvironment.swift
//  sky
//
//  Created by Diego Cavalcante on 22/08/2018.
//


import Foundation


enum HTTPEnvironment {
    case prod
    case dev
    
    var host : String {
        switch self {
        case .prod:
            return "https://sky-exercise.herokuapp.com/api/"
        case .dev:
            return "https://sky-exercise.herokuapp.com/api/"
        }
    }
}
