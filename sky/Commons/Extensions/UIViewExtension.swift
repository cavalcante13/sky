//
//  UIViewExtension.swift
//  Marsh
//
//  Created by Diego on 15/02/18.
//  Copyright © 2018 Diego. All rights reserved.
//

import Foundation

import UIKit

extension UIView {
    
    @IBInspectable
    var borderColor : UIColor? {
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var borderWidth : CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var cornerRadius : CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds      = newValue > 0
        }
    }
    
    @IBInspectable
    var isGone : Bool {
        get {
            return isHidden
        }
        set {
            self.alpha    = newValue ? 0.0 : 1.0
            self.isHidden = newValue
        }
    }
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func removeAllConstraints() {
        let constraints = self.superview?.constraints.filter { $0.firstItem === self || $0.secondItem === self } ?? []
        self.superview?.removeConstraints(constraints)
        self.removeConstraints(self.constraints)
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "position.x")
        animation.values = [0, 14, -14, 14, 0]
        animation.keyTimes =  [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
        animation.duration = 0.4
        animation.isAdditive = true
        animation.isRemovedOnCompletion = true
        self.layer.add(animation, forKey: "shake")
    }
}

extension UIView {
    static var nib : UINib {
        return UINib(nibName: String(describing : self), bundle: Bundle(for: self.classForCoder()))
    }
}

extension UIView {
    public func addShadow(ofColor color: UIColor = UIColor(red: 0.07, green: 0.47, blue: 0.57, alpha: 1.0), radius: CGFloat = 3, offset: CGSize = .zero, opacity: Float = 0.5) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.masksToBounds = false
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension UIView {
    
    func xib() {
        
        let bundle  = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let view = UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: self, options: nil).first as! UIView
        
        switch bounds {
        case .zero:
            frame = view.bounds
        default:
            view.frame = bounds
        }
        
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
    }
}
