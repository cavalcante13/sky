//
//  UITableViewExtension.swift
//  Ipeak
//
//  Created by Diego Costa on 04/08/18.
//  Copyright © 2018 Diego Costa. All rights reserved.
//

import Foundation
import UIKit


extension UITableView {
    
    
    func register<T : UITableViewCell>(class name : T.Type) {
        register(UINib(nibName: String(describing: name), bundle: Bundle(for: T.classForCoder())), forCellReuseIdentifier: String(describing: name))
    }
    
    func dequeue<T : UITableViewCell>(cell : T.Type, indexPath : IndexPath)-> T {
        return dequeueReusableCell(withIdentifier: String(describing: cell), for: indexPath) as! T
    }
}


