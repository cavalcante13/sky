//
//  UICollectionViewExtension.swift
//  Ipeak
//
//  Created by Diego Costa on 26/07/18.
//  Copyright © 2018 Diego Costa. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func dequeue<T : UICollectionViewCell>(cell : T.Type, indexPath : IndexPath)-> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: cell), for: indexPath) as! T
    }
    
    public func register<T: UICollectionViewCell>(class name: T.Type) {
        register(UINib(nibName: String(describing: name), bundle: Bundle(for: name.classForCoder())), forCellWithReuseIdentifier: String(describing: name))
    }
}
