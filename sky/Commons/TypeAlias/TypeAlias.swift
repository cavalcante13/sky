//
//  TypeAlias.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import Foundation


enum Result {
    case success(T : Codable)
}

typealias Action        = () -> (Void)
typealias ActionBool    = (Bool) -> (Void)
typealias ActionString  = (String) -> (Void)
typealias ActionInt     = (Int) -> (Void)

typealias ResultHandler = (Result)-> Swift.Void
typealias Handler       = ()-> Swift.Void
typealias Failure       = (NSError?)-> Swift.Void
