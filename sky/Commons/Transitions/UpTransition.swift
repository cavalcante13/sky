//
//  UpTransition.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import Foundation
import UIKit

class UpTransiction: NSObject, UIViewControllerAnimatedTransitioning {
    
    private var frame : CGRect = .zero
    
    
    init(frame : CGRect) {
        super.init()
        self.frame = frame
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let fromViewController  = transitionContext.viewController(forKey: .from),
            let toViewController    = transitionContext.viewController(forKey: .to),
            let snapshotView        = fromViewController.view.snapshotView(afterScreenUpdates: false) else { return }
        
        let containerView = transitionContext.containerView
        
        containerView.addSubview(snapshotView)
        containerView.addSubview(toViewController.view)
        
        containerView.insertSubview(toViewController.view, aboveSubview: snapshotView)
        
        snapshotView.frame = transitionContext.finalFrame(for: fromViewController)
        
        toViewController.view.frame.origin.y = transitionContext.finalFrame(for: toViewController).size.height
        
        containerView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        snapshotView.alpha = 0.3
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: [.curveEaseInOut], animations: {
            toViewController.view.frame              = self.frame
            toViewController.view.frame.size.height  -= self.frame.origin.y
            toViewController.view.frame.size.width   -= self.frame.origin.x
            snapshotView.alpha = 0.3
        }, completion: { _ in
            snapshotView.alpha = 0.3
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
