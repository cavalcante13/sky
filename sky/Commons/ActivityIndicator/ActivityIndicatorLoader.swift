//
//  ActivityIndicatorLoader.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import Foundation
import SVProgressHUD


class ActivityIndicator : NSObject {
    
    class func show() {
        SVProgressHUD.setBackgroundColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8971264983))
        SVProgressHUD.setForegroundColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        SVProgressHUD.show(withStatus: "Carregando...")
    }
    
    class func show(error message : String) {
        SVProgressHUD.setBackgroundColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8971264983))
        SVProgressHUD.setForegroundColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        SVProgressHUD.showError(withStatus: message)
    }
    
    class func stop() {
        SVProgressHUD.dismiss()
    }
}
