//
//  Movie.swift
//  sky
//
//  Created by Iterative on 23/08/18.
//  Copyright © 2018 DiegoCavalcante. All rights reserved.
//

import Foundation


struct Movie: Codable, Comparable {
    
    var id              : String?
    var title           : String?
    var overview        : String?
    var duration        : String?
    var releaseYear     : String?
    var coverStr        : String?
    var backdropsUrl    : [String] = []
    
    
    var coverUrl : URL? {
        guard let str = coverStr else {
            return nil
        }
        return URL(string: str)
    }
    
    private enum CodingKeys : String, CodingKey {
        case id             = "id"
        case title          = "title"
        case overview       = "overview"
        case duration       = "duration"
        case releaseYear    = "release_year"
        case coverStr       = "cover_url"
        case backdropsUrl   = "backdrops_url"
    }
    
    static func < (lhs: Movie, rhs: Movie) -> Bool {
        guard let leftId = lhs.id, let rightId = rhs.id else {
            return false
        }
        return leftId < rightId
    }
    
    static func == (lhs: Movie, rhs: Movie) -> Bool {
        return lhs.id == rhs.id
    }
}
